import { Icon, Link } from '@chakra-ui/react';
import { Link as RouterLink } from 'react-router-dom';

const HeaderMenuItem = ({ url, icon, label }) => {
	return (
		<Link
			as={RouterLink}
			to={url}
			fontSize='sm'
			fontWeight='semibold'
			letterSpacing='wide'
			textTransform='uppercase'
			mr='5'
			mt={{ base: '2', md: '0' }}
			display='flex'
			alignItems='center'
			color='whiteAlpha.800'
			_hover={{ textDecor: 'none', opacity: '0.7' }}>
			<Icon as={icon} mr='1' w='4' h='4' />
			{label}
		</Link>
	);
};

export default HeaderMenuItem;
